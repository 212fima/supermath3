QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_supermath3_test.cpp \
    main.cpp

HEADERS += \
    tst_supermath3_test.h
