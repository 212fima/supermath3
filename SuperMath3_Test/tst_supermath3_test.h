#ifndef TST_SUPERMATH3_TEST_H
#define TST_SUPERMATH3_TEST_H

#include <QtCore>
#include <QtTest/QtTest>

class laba_test : public QObject
{
    Q_OBJECT

public:
    laba_test();

private slots:
    void testLinear();
    void testSquare();
    void testCube();
    void testElemA();
    void testSumA();
    void testSumG();

};

#endif // TST_3LABA_TEST_H
