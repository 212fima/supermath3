/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_22;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_23;
    QDoubleSpinBox *linear_a_SpinBox;
    QLabel *label_24;
    QDoubleSpinBox *linear_b_SpinBox;
    QPushButton *linear_pushButton_2;
    QLabel *label_25;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_26;
    QDoubleSpinBox *sqr_a_SpinBox;
    QLabel *label_27;
    QDoubleSpinBox *sqr_b_SpinBox;
    QLabel *label_28;
    QDoubleSpinBox *sqr_c_SpinBox;
    QPushButton *sqr_pushButton_2;
    QLabel *label_29;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_30;
    QDoubleSpinBox *cube_a_SpinBox;
    QLabel *label_31;
    QDoubleSpinBox *cube_b_SpinBox;
    QLabel *label_32;
    QDoubleSpinBox *cube_c_SpinBox;
    QLabel *label_33;
    QDoubleSpinBox *cube_d_SpinBox;
    QPushButton *cube_pushButton_2;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_34;
    QRadioButton *randA_radioButton;
    QRadioButton *selfA_radioButton;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_35;
    QDoubleSpinBox *arithm_a_SpinBox;
    QLabel *label_36;
    QDoubleSpinBox *arithm_d_SpinBox;
    QLabel *label_37;
    QSpinBox *arithm_n_spinBox;
    QHBoxLayout *horizontalLayout_16;
    QPushButton *arithm_elem_pushButton_2;
    QPushButton *arithm_sum_pushButton_2;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_38;
    QRadioButton *randG_radioButton;
    QRadioButton *selfG_radioButton;
    QHBoxLayout *horizontalLayout_18;
    QLabel *label_39;
    QDoubleSpinBox *geom_b_SpinBox;
    QLabel *label_40;
    QDoubleSpinBox *geom_q_SpinBox;
    QLabel *label_41;
    QSpinBox *geom_n_spinBox;
    QHBoxLayout *horizontalLayout_19;
    QPushButton *geom_sum_pushButton_2;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QSpinBox *sum_a_spinBox;
    QLabel *label_3;
    QSpinBox *sum_b_spinBox;
    QLabel *label_4;
    QSpinBox *sum_c_spinBox;
    QLabel *label_5;
    QSpinBox *sum_d_spinBox;
    QPushButton *sum_pushButton;
    QLabel *label_6;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_7;
    QSpinBox *div_a_spinBox;
    QLabel *label_8;
    QSpinBox *div_b_spinBox;
    QLabel *label_9;
    QSpinBox *div_c_spinBox;
    QLabel *label_10;
    QSpinBox *div_d_spinBox;
    QPushButton *div_pushButton;
    QTextEdit *textEdit;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(756, 650);
        QFont font;
        font.setPointSize(9);
        MainWindow->setFont(font);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 0, 718, 638));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_22 = new QLabel(layoutWidget);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        QFont font1;
        font1.setPointSize(10);
        label_22->setFont(font1);

        verticalLayout_2->addWidget(label_22);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_23 = new QLabel(layoutWidget);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setFont(font);

        horizontalLayout_11->addWidget(label_23);

        linear_a_SpinBox = new QDoubleSpinBox(layoutWidget);
        linear_a_SpinBox->setObjectName(QString::fromUtf8("linear_a_SpinBox"));
        linear_a_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_11->addWidget(linear_a_SpinBox);

        label_24 = new QLabel(layoutWidget);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setFont(font);

        horizontalLayout_11->addWidget(label_24);

        linear_b_SpinBox = new QDoubleSpinBox(layoutWidget);
        linear_b_SpinBox->setObjectName(QString::fromUtf8("linear_b_SpinBox"));
        linear_b_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_11->addWidget(linear_b_SpinBox);


        verticalLayout_2->addLayout(horizontalLayout_11);

        linear_pushButton_2 = new QPushButton(layoutWidget);
        linear_pushButton_2->setObjectName(QString::fromUtf8("linear_pushButton_2"));
        linear_pushButton_2->setFont(font);

        verticalLayout_2->addWidget(linear_pushButton_2);

        label_25 = new QLabel(layoutWidget);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setFont(font1);

        verticalLayout_2->addWidget(label_25);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_26 = new QLabel(layoutWidget);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setFont(font);

        horizontalLayout_12->addWidget(label_26);

        sqr_a_SpinBox = new QDoubleSpinBox(layoutWidget);
        sqr_a_SpinBox->setObjectName(QString::fromUtf8("sqr_a_SpinBox"));
        sqr_a_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_12->addWidget(sqr_a_SpinBox);

        label_27 = new QLabel(layoutWidget);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setFont(font);

        horizontalLayout_12->addWidget(label_27);

        sqr_b_SpinBox = new QDoubleSpinBox(layoutWidget);
        sqr_b_SpinBox->setObjectName(QString::fromUtf8("sqr_b_SpinBox"));
        sqr_b_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_12->addWidget(sqr_b_SpinBox);

        label_28 = new QLabel(layoutWidget);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setFont(font);

        horizontalLayout_12->addWidget(label_28);

        sqr_c_SpinBox = new QDoubleSpinBox(layoutWidget);
        sqr_c_SpinBox->setObjectName(QString::fromUtf8("sqr_c_SpinBox"));
        sqr_c_SpinBox->setMinimum(-99.000000000000000);

        horizontalLayout_12->addWidget(sqr_c_SpinBox);


        verticalLayout_2->addLayout(horizontalLayout_12);

        sqr_pushButton_2 = new QPushButton(layoutWidget);
        sqr_pushButton_2->setObjectName(QString::fromUtf8("sqr_pushButton_2"));
        sqr_pushButton_2->setFont(font);

        verticalLayout_2->addWidget(sqr_pushButton_2);

        label_29 = new QLabel(layoutWidget);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setFont(font1);

        verticalLayout_2->addWidget(label_29);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_30 = new QLabel(layoutWidget);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setFont(font);

        horizontalLayout_13->addWidget(label_30);

        cube_a_SpinBox = new QDoubleSpinBox(layoutWidget);
        cube_a_SpinBox->setObjectName(QString::fromUtf8("cube_a_SpinBox"));
        cube_a_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_13->addWidget(cube_a_SpinBox);

        label_31 = new QLabel(layoutWidget);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setFont(font);

        horizontalLayout_13->addWidget(label_31);

        cube_b_SpinBox = new QDoubleSpinBox(layoutWidget);
        cube_b_SpinBox->setObjectName(QString::fromUtf8("cube_b_SpinBox"));
        cube_b_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_13->addWidget(cube_b_SpinBox);

        label_32 = new QLabel(layoutWidget);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setFont(font);

        horizontalLayout_13->addWidget(label_32);

        cube_c_SpinBox = new QDoubleSpinBox(layoutWidget);
        cube_c_SpinBox->setObjectName(QString::fromUtf8("cube_c_SpinBox"));
        cube_c_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_13->addWidget(cube_c_SpinBox);

        label_33 = new QLabel(layoutWidget);
        label_33->setObjectName(QString::fromUtf8("label_33"));
        label_33->setFont(font);

        horizontalLayout_13->addWidget(label_33);

        cube_d_SpinBox = new QDoubleSpinBox(layoutWidget);
        cube_d_SpinBox->setObjectName(QString::fromUtf8("cube_d_SpinBox"));
        cube_d_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_13->addWidget(cube_d_SpinBox);


        verticalLayout_2->addLayout(horizontalLayout_13);

        cube_pushButton_2 = new QPushButton(layoutWidget);
        cube_pushButton_2->setObjectName(QString::fromUtf8("cube_pushButton_2"));
        cube_pushButton_2->setFont(font);

        verticalLayout_2->addWidget(cube_pushButton_2);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_34 = new QLabel(layoutWidget);
        label_34->setObjectName(QString::fromUtf8("label_34"));
        label_34->setFont(font1);

        horizontalLayout_14->addWidget(label_34);

        randA_radioButton = new QRadioButton(layoutWidget);
        randA_radioButton->setObjectName(QString::fromUtf8("randA_radioButton"));
        randA_radioButton->setFont(font);
        randA_radioButton->setChecked(false);

        horizontalLayout_14->addWidget(randA_radioButton);

        selfA_radioButton = new QRadioButton(layoutWidget);
        selfA_radioButton->setObjectName(QString::fromUtf8("selfA_radioButton"));
        selfA_radioButton->setFont(font);
        selfA_radioButton->setChecked(true);

        horizontalLayout_14->addWidget(selfA_radioButton);


        verticalLayout_2->addLayout(horizontalLayout_14);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        label_35 = new QLabel(layoutWidget);
        label_35->setObjectName(QString::fromUtf8("label_35"));
        label_35->setFont(font);

        horizontalLayout_15->addWidget(label_35);

        arithm_a_SpinBox = new QDoubleSpinBox(layoutWidget);
        arithm_a_SpinBox->setObjectName(QString::fromUtf8("arithm_a_SpinBox"));
        arithm_a_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_15->addWidget(arithm_a_SpinBox);

        label_36 = new QLabel(layoutWidget);
        label_36->setObjectName(QString::fromUtf8("label_36"));
        label_36->setFont(font);

        horizontalLayout_15->addWidget(label_36);

        arithm_d_SpinBox = new QDoubleSpinBox(layoutWidget);
        arithm_d_SpinBox->setObjectName(QString::fromUtf8("arithm_d_SpinBox"));
        arithm_d_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_15->addWidget(arithm_d_SpinBox);

        label_37 = new QLabel(layoutWidget);
        label_37->setObjectName(QString::fromUtf8("label_37"));
        label_37->setFont(font);

        horizontalLayout_15->addWidget(label_37);

        arithm_n_spinBox = new QSpinBox(layoutWidget);
        arithm_n_spinBox->setObjectName(QString::fromUtf8("arithm_n_spinBox"));
        arithm_n_spinBox->setMinimum(1);

        horizontalLayout_15->addWidget(arithm_n_spinBox);


        verticalLayout_2->addLayout(horizontalLayout_15);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        arithm_elem_pushButton_2 = new QPushButton(layoutWidget);
        arithm_elem_pushButton_2->setObjectName(QString::fromUtf8("arithm_elem_pushButton_2"));
        arithm_elem_pushButton_2->setFont(font);

        horizontalLayout_16->addWidget(arithm_elem_pushButton_2);

        arithm_sum_pushButton_2 = new QPushButton(layoutWidget);
        arithm_sum_pushButton_2->setObjectName(QString::fromUtf8("arithm_sum_pushButton_2"));
        arithm_sum_pushButton_2->setFont(font);

        horizontalLayout_16->addWidget(arithm_sum_pushButton_2);


        verticalLayout_2->addLayout(horizontalLayout_16);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        label_38 = new QLabel(layoutWidget);
        label_38->setObjectName(QString::fromUtf8("label_38"));
        label_38->setFont(font1);

        horizontalLayout_17->addWidget(label_38);

        randG_radioButton = new QRadioButton(layoutWidget);
        randG_radioButton->setObjectName(QString::fromUtf8("randG_radioButton"));
        randG_radioButton->setChecked(false);

        horizontalLayout_17->addWidget(randG_radioButton);

        selfG_radioButton = new QRadioButton(layoutWidget);
        selfG_radioButton->setObjectName(QString::fromUtf8("selfG_radioButton"));
        selfG_radioButton->setChecked(true);

        horizontalLayout_17->addWidget(selfG_radioButton);


        verticalLayout_2->addLayout(horizontalLayout_17);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        label_39 = new QLabel(layoutWidget);
        label_39->setObjectName(QString::fromUtf8("label_39"));
        label_39->setFont(font);

        horizontalLayout_18->addWidget(label_39);

        geom_b_SpinBox = new QDoubleSpinBox(layoutWidget);
        geom_b_SpinBox->setObjectName(QString::fromUtf8("geom_b_SpinBox"));
        geom_b_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_18->addWidget(geom_b_SpinBox);

        label_40 = new QLabel(layoutWidget);
        label_40->setObjectName(QString::fromUtf8("label_40"));
        label_40->setFont(font);

        horizontalLayout_18->addWidget(label_40);

        geom_q_SpinBox = new QDoubleSpinBox(layoutWidget);
        geom_q_SpinBox->setObjectName(QString::fromUtf8("geom_q_SpinBox"));
        geom_q_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_18->addWidget(geom_q_SpinBox);

        label_41 = new QLabel(layoutWidget);
        label_41->setObjectName(QString::fromUtf8("label_41"));
        label_41->setFont(font);

        horizontalLayout_18->addWidget(label_41);

        geom_n_spinBox = new QSpinBox(layoutWidget);
        geom_n_spinBox->setObjectName(QString::fromUtf8("geom_n_spinBox"));
        geom_n_spinBox->setMinimum(1);

        horizontalLayout_18->addWidget(geom_n_spinBox);


        verticalLayout_2->addLayout(horizontalLayout_18);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        geom_sum_pushButton_2 = new QPushButton(layoutWidget);
        geom_sum_pushButton_2->setObjectName(QString::fromUtf8("geom_sum_pushButton_2"));
        geom_sum_pushButton_2->setFont(font);

        horizontalLayout_19->addWidget(geom_sum_pushButton_2);


        verticalLayout_2->addLayout(horizontalLayout_19);

        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font1);

        verticalLayout_2->addWidget(label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        horizontalLayout->addWidget(label_2);

        sum_a_spinBox = new QSpinBox(layoutWidget);
        sum_a_spinBox->setObjectName(QString::fromUtf8("sum_a_spinBox"));
        sum_a_spinBox->setMinimum(1);

        horizontalLayout->addWidget(sum_a_spinBox);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);

        horizontalLayout->addWidget(label_3);

        sum_b_spinBox = new QSpinBox(layoutWidget);
        sum_b_spinBox->setObjectName(QString::fromUtf8("sum_b_spinBox"));
        sum_b_spinBox->setMinimum(1);

        horizontalLayout->addWidget(sum_b_spinBox);

        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font);

        horizontalLayout->addWidget(label_4);

        sum_c_spinBox = new QSpinBox(layoutWidget);
        sum_c_spinBox->setObjectName(QString::fromUtf8("sum_c_spinBox"));
        sum_c_spinBox->setMinimum(1);

        horizontalLayout->addWidget(sum_c_spinBox);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font);

        horizontalLayout->addWidget(label_5);

        sum_d_spinBox = new QSpinBox(layoutWidget);
        sum_d_spinBox->setObjectName(QString::fromUtf8("sum_d_spinBox"));
        sum_d_spinBox->setMinimum(1);

        horizontalLayout->addWidget(sum_d_spinBox);


        verticalLayout_2->addLayout(horizontalLayout);

        sum_pushButton = new QPushButton(layoutWidget);
        sum_pushButton->setObjectName(QString::fromUtf8("sum_pushButton"));
        sum_pushButton->setFont(font);

        verticalLayout_2->addWidget(sum_pushButton);

        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font1);

        verticalLayout_2->addWidget(label_6);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_7 = new QLabel(layoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font);

        horizontalLayout_2->addWidget(label_7);

        div_a_spinBox = new QSpinBox(layoutWidget);
        div_a_spinBox->setObjectName(QString::fromUtf8("div_a_spinBox"));
        div_a_spinBox->setMinimum(1);

        horizontalLayout_2->addWidget(div_a_spinBox);

        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font);

        horizontalLayout_2->addWidget(label_8);

        div_b_spinBox = new QSpinBox(layoutWidget);
        div_b_spinBox->setObjectName(QString::fromUtf8("div_b_spinBox"));
        div_b_spinBox->setMinimum(1);

        horizontalLayout_2->addWidget(div_b_spinBox);

        label_9 = new QLabel(layoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font);

        horizontalLayout_2->addWidget(label_9);

        div_c_spinBox = new QSpinBox(layoutWidget);
        div_c_spinBox->setObjectName(QString::fromUtf8("div_c_spinBox"));
        div_c_spinBox->setMinimum(1);

        horizontalLayout_2->addWidget(div_c_spinBox);

        label_10 = new QLabel(layoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font);

        horizontalLayout_2->addWidget(label_10);

        div_d_spinBox = new QSpinBox(layoutWidget);
        div_d_spinBox->setObjectName(QString::fromUtf8("div_d_spinBox"));
        div_d_spinBox->setMinimum(1);

        horizontalLayout_2->addWidget(div_d_spinBox);


        verticalLayout_2->addLayout(horizontalLayout_2);

        div_pushButton = new QPushButton(layoutWidget);
        div_pushButton->setObjectName(QString::fromUtf8("div_pushButton"));

        verticalLayout_2->addWidget(div_pushButton);

        textEdit = new QTextEdit(layoutWidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));

        verticalLayout_2->addWidget(textEdit);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label_22->setText(QApplication::translate("MainWindow", "1) \320\240\320\265\321\210\320\265\320\275\320\270\320\265 \320\273\320\270\320\275\320\265\320\271\320\275\320\276\320\263\320\276 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\321\217: ax + b = 0", nullptr));
        label_23->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 a:", nullptr));
        label_24->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 b:", nullptr));
        linear_pushButton_2->setText(QApplication::translate("MainWindow", "\320\240\320\265\321\210\320\270\321\202\321\214 \320\273\320\270\320\275\320\265\320\271\320\275\320\276\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265", nullptr));
        label_25->setText(QApplication::translate("MainWindow", "2) \320\240\320\265\321\210\320\265\320\275\320\270\320\265 \320\272\320\262\320\260\320\264\321\200\320\260\321\202\320\275\320\276\320\263\320\276 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\321\217: ax^2 + bx + c = 0", nullptr));
        label_26->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 a:", nullptr));
        label_27->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 b:", nullptr));
        label_28->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 c:", nullptr));
        sqr_pushButton_2->setText(QApplication::translate("MainWindow", "\320\240\320\265\321\210\320\270\321\202\321\214 \320\272\320\262\320\260\320\264\321\200\320\260\321\202\320\275\320\276\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265", nullptr));
        label_29->setText(QApplication::translate("MainWindow", "3) \320\240\320\265\321\210\320\265\320\275\320\270\320\265 \320\272\321\203\320\261\320\270\321\207\320\265\321\201\320\272\320\276\320\263\320\276 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\321\217: ax^3 + bx^2 + cx + d = 0", nullptr));
        label_30->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 a:", nullptr));
        label_31->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 b:", nullptr));
        label_32->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 c:", nullptr));
        label_33->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 d:", nullptr));
        cube_pushButton_2->setText(QApplication::translate("MainWindow", "\320\240\320\265\321\210\320\270\321\202\321\214 \320\272\321\203\320\261\320\270\321\207\320\265\321\201\320\272\320\276\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265", nullptr));
        label_34->setText(QApplication::translate("MainWindow", "4) \320\227\320\260\320\264\320\260\321\202\321\214 \320\260\321\200\320\270\321\204\320\274\320\265\321\202\320\270\321\207\320\265\321\201\320\272\321\203\321\216 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\321\216", nullptr));
        randA_radioButton->setText(QApplication::translate("MainWindow", "\321\201\320\273\321\203\321\207\320\260\320\271\320\275\320\276", nullptr));
        selfA_radioButton->setText(QApplication::translate("MainWindow", "\320\262\321\200\321\203\321\207\320\275\321\203\321\216", nullptr));
        label_35->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\275\320\260\321\207\320\260\320\273\321\214\320\275\321\213\320\271 \321\215\320\273\320\265\320\274\320\265\320\275\321\202:", nullptr));
        label_36->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\200\320\260\320\267\320\275\320\276\321\201\321\202\321\214 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\320\270: ", nullptr));
        label_37->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\275\320\276\320\274\320\265\321\200 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\260:", nullptr));
        arithm_elem_pushButton_2->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214 \320\262\321\213\320\261\321\200\320\260\320\275\320\275\321\213\320\271 \321\215\320\273\320\265\320\274\320\265\320\275\321\202", nullptr));
        arithm_sum_pushButton_2->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214 \321\201\321\203\320\274\320\274\321\203 \320\264\320\276 \320\262\321\213\320\261\321\200\320\260\320\275\320\275\320\276\320\263\320\276 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\260", nullptr));
        label_38->setText(QApplication::translate("MainWindow", "5) \320\227\320\260\320\264\320\260\321\202\321\214 \320\263\320\265\320\276\320\274\320\265\321\202\321\200\320\270\321\207\320\265\321\201\320\272\321\203\321\216 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\321\216", nullptr));
        randG_radioButton->setText(QApplication::translate("MainWindow", "\321\201\320\273\321\203\321\207\320\260\320\271\320\275\320\276", nullptr));
        selfG_radioButton->setText(QApplication::translate("MainWindow", "\320\262\321\200\321\203\321\207\320\275\321\203\321\216", nullptr));
        label_39->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\275\320\260\321\207\320\260\320\273\321\214\320\275\321\213\320\271 \321\215\320\273\320\265\320\274\320\265\320\275\321\202:", nullptr));
        label_40->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\267\320\275\320\260\320\274\320\265\320\275\320\260\321\202\320\265\320\273\321\214 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\320\270: ", nullptr));
        label_41->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\275\320\276\320\274\320\265\321\200 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\260:", nullptr));
        geom_sum_pushButton_2->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214 \321\201\321\203\320\274\320\274\321\203 \320\262\321\201\320\265\321\205 \321\207\320\273\320\265\320\275\320\276\320\262 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\320\270", nullptr));
        label->setText(QApplication::translate("MainWindow", "6) \320\222\321\213\321\207\320\270\321\201\320\273\320\265\320\275\320\270\320\265 \321\201\321\203\320\274\320\274\321\213 \320\276\320\261\321\213\320\272\320\275\320\276\320\262\320\265\320\275\320\275\321\213\321\205 \320\264\321\200\320\276\320\261\320\265\320\271: a/b + c/d", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 a:", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 b:", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 c:", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 d:", nullptr));
        sum_pushButton->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214 \321\201\321\203\320\274\320\274\321\203", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "7) \320\235\320\260\321\205\320\276\320\266\320\264\320\265\320\275\320\270\320\265 \321\207\320\260\321\201\321\202\320\275\320\276\320\263\320\276 \320\276\320\261\321\213\320\272\320\275\320\276\320\262\320\265\320\275\320\275\321\213\321\205 \320\264\321\200\320\276\320\261\320\265\320\271: a/b : c/d ", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 a:", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 b:", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 c:", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 d:", nullptr));
        div_pushButton->setText(QApplication::translate("MainWindow", "\320\235\320\260\320\271\321\202\320\270 \321\207\320\260\321\201\321\202\320\275\320\276\320\265", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
