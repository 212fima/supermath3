/**
*\file
*\brief
Модуль программы
*\author Самый уставший на свете Александр потому-что у него не запускается раннер в 20 раз
*\version 1.0
*
* Функции, необходимые для базовых математических операций
*/
#ifndef FUNTIK_H
#define FUNTIK_H


#include <QVector>
#include <QtMath>

/**
 * @brief linearEquation - Функция вычисляющая корень линейного уравнения
 * @param a - Коэфицент при x
 * @param b - Свободный член
 * @return  - Искомый корень
 */
double linearEquation(double a, double b);

/**
 * @brief squareEquation - Функция для вычисления действительных корней квадратного уравнения
 * @param a - Коэфицент при x^2
 * @param b - Коэфицент при x
 * @param c - Свободный член
 * @return - Искомые корни
 */
QVector<double> squareEquation(double a, double b, double c);

/**
 * @brief cubeEquation - Функция для вычисления действительных корней кубического уравнения
 * @param a1 - Коэфицент при x^3
 * @param b1 - Коэфицент при x^2
 * @param c1 - Коэфицент при x
 * @param d1 - Свободный член
 * @return  - Корни уравнения
 */
QVector<double> cubeEquation(double a1, double b1, double c1, double d1);

/**
 * @brief elemArithProgress - Функция нахождения н-ого члена арифмитической прогрессии
 * @param n - Номер искомого члена
 * @param a - Первый член прогрессии
 * @param d - Разность прогресии
 * @return - Искомый элемент
 */
double elemArithProgress(int n, double a, double d);

/**
 * @brief sumArithProgress - Функция нахождения суммы арифметической прогрессии
 * @param n - Колличество элементов
 * @param a - Значение первого члена прогресии
 * @param d - Разность прогрессии
 * @return - Искомая сумма
 */
double sumArithProgress(int n, double a, double d);
/**
 * @brief sumGeomProgress - Функция нахождения суммы геометрической прогрессии
 * @param n - Колличество элементов
 * @param b - Значение первого члена прогресии
 * @param q - Разность прогрессии
 * @return  - Искомая сумма
 */
double sumGeomProgress(int n, double b, double q);

/**
 * @brief sumOrdFrac - функция нахождения суммы обыкновенных дробей
 * @param a - числитель первой дроби
 * @param b - знаменатель перввой дроби
 * @param c - числитель  второй дроби
 * @param d - знаменатель второй дроби
 * @return - искомая сумма
 */
double sumOrdFrac(int a, int b, int c, int d);

/**
 * @brief divOrdFrac - функция нахождения часного обыкновенных дробей
 * @param a - числитель первой дроби
 * @param b - знаменатель перввой дроби
 * @param c - числитель  второй дроби
 * @param d - знаменатель второй дроби
 * @return - искомое частное
 */
double divOrdFrac(int a, int b, int c, int d);

#endif // FUNTIK_H
